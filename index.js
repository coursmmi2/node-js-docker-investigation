let resinDockerBuild = require('resin-docker-build');
const {docker, imageMgr, dockerComposer, dockerUtils} = require('mydockerjs');
const builder = resinDockerBuild.Builder.fromDockerOpts({ socketPath: '/var/run/docker.sock' });

let projectUuid = "uuid1";
let projectDir = "projects";
let globalImageId;

// build image
const hooks = {
    buildStream: (stream) => { stream.pipe(process.stdout); },
    buildSuccess: runImage,
    buildFailure: (error) => { console.error(`Error building container: ${error}`); }
}

builder.buildDir('./images', {}, hooks);

// Run image
function runImage(imageId, layers) {
    globalImageId = imageId;
    console.log("[runImage]");
    docker.run(imageId, function (err, data) {
        console.log("[run]");
        if (err) {
            console.error("[docker.run]:", "err:" + err, "data:", data);
            return
        }
        console.log("runned image");
    }, {
        rm: true,
        volumes: [{ hostPath: `${__dirname}/${projectDir}/${projectUuid}`, containerPath: "/usr/share/nginx" }],
        ports: {'80' : '70' }
    });
}

process.on('SIGINT', function () {
    imageMgr.removeImage(globalImageId, "", function () {
        console.log("[remove image done]")
    });

    imageMgr.removeUntagged( function () {
        console.log("removed untages")
    });
});


