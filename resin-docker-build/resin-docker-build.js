let resinDockerBuild = require('resin-docker-build');
 
// building an image
const builder = resinDockerBuild.Builder.fromDockerOpts({ socketPath: '/var/run/docker.sock' });
 
const hooks = {
    buildStream: (stream) => {
        stream.pipe(process.stdout);
    },
    buildSuccess: (imageId, layers) => {
        console.log(`Successful build! ImageId: ${imageId}`);
    },
    buildFailure: (error) => {
        console.error(`Error building container: ${error}`);
    }
}
 
builder.buildDir('./images', {}, hooks);