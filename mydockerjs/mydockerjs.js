// Utils
// const du = require('mydockerjs').dockerUtils;

// du.isDockerEngineRunning((err, isRunning) => {console.log("isRunning", isRunning);});

// let dockerCliInstalled = du.isDockerCliInstalledSync();
// console.log("dockerCliInstalled", dockerCliInstalled);

// let dockerComposeInstalled = du.isDockerComposeInstalledSync();
// console.log("dockerComposeInstalled", dockerComposeInstalled);


// Image
// const dockerImages = require('mydockerjs').imageMgr;

// dockerImages.getDetailedList(function(err, data) {
//     if(err) console.erro(err);
//     console.log(data.map((image)=>image.name));
// });

//Get image names  
// dockerImages.getNames(function (err, json) {
//     console.log(err, json)
// }, { onlytagged: true });

// dockerImages.getDetailedList(function (err, data) {
//     console.log(err, data);
// })

// dockerImages.getJSONList(function(err, data) {
//     console.log(err, data);
//   })

// Image library
const dockerJS = require('mydockerjs').docker;

dockerJS.ps(function (err, dockerContainers) {
    if (err) console.log(err)
    else console.log("[ps]: ",dockerContainers);
});

dockerJS.run('nodejs', function (err, data) {
    if (err) {
        console.log("Some err:", err);
        console.log(data);
    }
    else {
        console.log("[run]", data);
        dockerJS.ps(function (err, dockerContainers) {
            if (err) console.log(err)
            else console.log("[ps]: ",dockerContainers);
        });
    }
});

// dockerJS.exec(nameContainer, command, callback, paramsInput);

// var flags = {
//     driver: 'bridge',
//     subnet: '192.168.1.1/24'
// }

// dockerJS.createNetwork("testRete", function (err, data) {
//     utils.print(err, data)
// }, flags) 

// var name = 'testRete';

// dockerJS.removeNetwork(name, utils.print)